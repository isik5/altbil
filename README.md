# Alternatif Bilişim Bilgi Mimarisi

#### Hakkımızda

- [Amaçlar](https://www.alternatifbilisim.org/wiki/Ama%C3%A7lar)
- [Tüzük](https://www.alternatifbilisim.org/wiki/Alternatif_Bili%C5%9Fim_Derne%C4%9Fi_T%C3%BCz%C3%BC%C4%9F%C3%BC)
- Kimler (Yönetim Kurulu, Denetim Kurulu, Danışma Kurulu, roller)
- Faaliyetler
    - Kampanyalar
    - Bildiriler
    - Etkinlikler (konferans, hafta, atölye)
    - Raporlar
    - Desteklenen Kampanyalar
    - Katılım gösterilen etkinlikler
    - Danışmanlık
    - Yasa taslak önerileri
    - Medyada Alternatif Bilişim

#### İletişim

- Adres: Alternatif Bilişim Derneği Genel Merkezi, Eğitim mah. Muratpaşa cad. Alpay apt. No:11/9  Kadıköy/İstanbul
- E-posta: bilgi@alternatifbilisim.org
- [E-posta Listesi](http://liste.alternatifbilisim.org/listinfo/ab)
- [Twitter](https://twitter.com/altbilisim)
- [Facebook](https://www.facebook.com/AltBilisim)

#### Üyelik

- [Üye Formu](https://alternatifbilisim.org/files/uyelik_formu.pdf)
- [Üyelik aidatı elektronik ödeme - Sanal POS](https://odeme.alternatifbilisim.org/)
- Üyelik şartları
    - Form tam ve doğru bilgilerle doldurulmuş olmalı.
    - Üye alınacak kişi en az bir dernek üyesi referansı ile önerilebilir.
    - Üye kabulünden önce 60 lira giriş ödentisi ve aylık 20 lira aidatın birlikte gönderilir (toplam 80TL).
    - Öğrenciler için giriş ödentisi olan 24 lira + 8 lira aylık aidatdır.
    - Dernek çalışmalarında hiç varlık göstermeyen ve aidat ödemeyen kişiler tüzük gereği çıkarılır.

#### Bağış

- [Dernek banka bilgisi](https://www.alternatifbilisim.org/wiki/Banka_Hesap_Bilgileri)
- [Kredi kartıyla online ödeme](https://odeme.alternatifbilisim.org/)

#### Çalışma Grupları

Katılmak isteyenler grupta adı geçenlerden birilerine ilk epostayı atarak katılabilir.
6 ayda bir fiziki olarak bir araya gelinecek.
Toplantılar gelmek isteyen tüm üyelere açık, birikimi/deneyimi itibariyle bu toplantılarda özellikle yönetim, denetim, ve danışma kurulu üyelerimizin katılması bekleniyor.

##### Kitlesel gözetim [Ayşe, Barış, Melih, Işık, Neslihan, Mutlu, Seda, Halil]

- Gözetim
- Sansür
- Mahremiyet

##### Özgür Lisanslar [Barış, Ayşe, Melih, Neslihan, Halil]

- choosealicense.com > lisanssec.org
- altbil onayli sertifika

##### Kullanıcı Sözleşmeleri takibi [Nermin, Neslihan, Asli, İlden, Barış]

- tosdr.org
- Turkcell, Avea, Vodafone, Turktelekom, ttnet, superonline ...
- [Çalışma grubu e-posta listesi](https://liste.alternatifbilisim.org/listinfo/tosdr-tr)

##### Ağ tarafsızlığı [Oğuz, Burak, Gamze]

- İnternet altyapısında fiili tekelin sonlandırılması
- Telekomda şirket / yatırım ortaklıkları

##### İnternet yönetişimi [Aslı, Burak, Seda, Işık, Melih, Coşkunoğlu]
- Ungov Forumları [Burak, Zeyno, Işık, Aslı]
    - Türkiye https://iuf.alternatifbilisim.org
    - Brezilya http://iuf.partidopirata.org
    - İtalya http://iufitalia.net/
- Internet Social Forum  http://www.internetsocialforum.net

##### Siberfeminizm [Ayşe, Zeyno, İlden, Gamze, Aslı, Işık, Neslihan, Nermin, Seda]

- Dijital Dantel
- Kadınlar Makinası
- [Ada Lovelace Günü](https://tr.wikipedia.org/wiki/Ada_Lovelace)

##### Dijital emek / müşterek veri [Burak, Oğuz, Aslı, Seda, Zeynep Özarslan, Ahmet Sabancı]

- http://userlabor.org
- http://digitallabor.org

##### Kişisel veri mahremiyetinin korunması [Melih, Burak, Coşkunoğlu, Mutlu, Seda]

- Sağlık alanında http://www.kisiselsaglikverileri.org/
- Telekomünikasyon alanında

##### Kamu hakkında ve kamuya ait verilerin şeffaflığı [Burak, Mutlu]

- Bilgi Edinme Hakkının etkin kullanımı
- Özgür veri standartlarının sivil / kamuda kullanabileceği araçlar

##### Taahhütname ve meclis grubu [Çoşkunoğlu, Melih, Ali Riza]

- [2015 Haziran seçimi için hazırlanan taahhütname](https://github.com/AlternatifBilisim/altbil/blob/master/taahhutname.md)

##### İletişim ve Organizasyon [Erdem, Oguz, Burak, Orkut]

- Basın mensupları iletişim bilgilerinin yeniden derlenmesi [Alp, İlden...]
- Sosyal medya hesaplarının yönetilmesi [Işık M, Burak, Barış, Erdem]
- Duyuru listesi, Mailchimp benzeri bir bülten entegrasyonu

##### Teknik Operasyonlar [Gökhan, Oğuz, Burak, Ali Rıza, Barış, Halil]

- Dernek sitesi giriş sayfası EN/TR
- Üyelik yönetim sistemi
- Bağışlar için ödeme otomatizasyonu
- Proje yönetim sistemi
- İçerik Yönetim Sistemi

##### İç eğitimle özgür yazılıma geçiş [Barış, Neslihan, Uğurcan, Işık, Halil]

##### Çocuklara özgür yazılım [Nermin, Aslı]

##### İnternet'in Durumu Rapor grubu [Zeyno, Gamze, İlden, Alp, Mutlu, Zeynep Özarslan]

##### Yeni Medya Kongre DK Ekibi [Mutlu, Günseli, Tuğrul, Aslı, Zeynep, Perrin]

##### Güvenli haberleşme / Kriptoloji [Ugurcan, Barış, Neslihan, Işık, Alp, Seda]


#### Aktif Projeler
- [KemGözlereŞiş](https://kemgozleresis.org.tr)
- [Yenimedya.org.tr](http://yenimedya.org.tr)
- [Ekitap](http://ekitap.alternatifbilisim.org)
- [Netdefteri](http://netdefteri.alternatifbilisim.org)
- [Bildirge.org](http://bildirge.org)
- [Internet Ungovernance Forum](https://iuf.alternatifbilisim.org)
- [Https Kullan](https://httpskullan.org)
- [Aletetme.org](http://aletetme.org)

#### Sitesi Çalışmayan Geçmiş Projeler
- [Altbilisim TV](http://tv.alternatifbilisim.org) (Çalışmıyor)
- [Enphormasyon.org](http://enphormasyon.org) (Çalışmıyor)
- [Yeniden Başlat](https://yenidenbaslat.org) (Çalışmıyor)
- [Taahhutname.org](http://taahhutname.org) (Bu proje tamamlanamadi.)
